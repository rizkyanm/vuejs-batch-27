// soal 1 Function Penghitung Jumlah Kata
function jumlah_kata(kalimat){
    var arrayKata = kalimat.split(" ");
    var arrayKata = arrayKata.filter(function(kata){
        return kata != "";
    })
    // console.log(arrayKata);
    return arrayKata.length

}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var jumlahKata1 = jumlah_kata(kalimat_1);
var jumlahKata2 = jumlah_kata(kalimat_2);
console.log(jumlahKata1);
console.log(jumlahKata2);



//soal 2 : Function Penghasil Tanggal Hari Esok
var arrayHari = [31,28,31,30,31,30, 31,31,30,31,30,31];
var arrayBulan =['Januari', 'Februari','Maret','April','Mei','Juni','Juli',
'Agustus', 'September', 'Oktober', 'November', 'Desember'];

function next_date(tanggal, bulan, tahun){
    
    var next_day =0;
    var next_month = '';
    var next_year = 0;
    var cekJumlahHari = 0;

    //Tahun Kabisat
    if (tahun % 4 === 0 && bulan === 2){
        next_month = arrayBulan[bulan-1];
        next_year = tahun;

            if(tanggal <= arrayHari[bulan-1]){
                next_day = tanggal+1;
            }else{
                next_day= 1;
                next_month = arrayBulan[bulan]
            }

    }else if (bulan === 12 && tanggal === 31){ //akhir tahun
        next_day = 1;
        next_month = arrayBulan[0];
        next_year = tahun+1;

    }else{ 

        cekJumlahHari = arrayHari[bulan];
        if (tanggal < cekJumlahHari){
            next_day = tanggal +1;
            next_month = arrayBulan[bulan-1];
            next_year = tahun;
        }else{
            next_day = 1;
            next_month = arrayBulan[bulan];
            next_year = tahun;
        }
    
}
var next_date = next_day+" "+next_month+" "+next_year;
return (next_date);
};

var date1 = next_date(29, 2, 2020);
var date2 = next_date(28, 2, 2020);
var date3 = next_date(31, 12, 2020);
var date4 = next_date(31, 5, 2020);
console.log(date1);
console.log(date2);
console.log(date3);
console.log(date4);