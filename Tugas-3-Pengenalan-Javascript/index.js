var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

//soal 1
var saya =  pertama.substring(0,5);
var senang =  pertama.substring(12,19);
var belajar = kedua.substring(0,8);
var upperJs = kedua.substring(8,19).toUpperCase();
console.log(saya, senang, belajar, upperJs) ; // print dengan pemisah ,
console.log(saya.concat(senang).concat(belajar).concat (upperJs)); // print dengan concats



// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

IntPertama = Number(kataPertama);
IntKedua = Number(kataKedua);
IntKetiga = Number(kataKetiga);
IntKeempat = Number(kataKeempat);

console.log(IntPertama + IntKedua * IntKetiga + IntKeempat);



//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14); 
var kataKetiga = kalimat.substring(15,18);
var kataKeempat= kalimat.substring(19,24); 
var kataKelima = kalimat.substring(25,31); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);