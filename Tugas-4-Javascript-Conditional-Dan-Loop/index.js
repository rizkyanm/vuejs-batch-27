//soal 1
var nilai = 90;
var indeksNilai = '';
if (nilai >= 85) {
    indeksNilai = 'A';
} else if (nilai >= 75 && nilai < 85) {
    indeksNilai = 'B';
} else if (nilai >= 65 && nilai < 75) {
    indeksNilai = 'C';
} else if (nilai >= 55 && nilai < 65) {
    indeksNilai = 'D';
} else if (nilai < 55) {
    indeksNilai = 'E';
}

console.log(indeksNilai);


//soal 2
var tanggal = 16;
var bulan = 5;
var tahun = 1994;

switch (bulan) {
    case 1: { console.log(tanggal, "Januari", tahun); break; }
    case 2: { console.log(tanggal, "Februari", tahun); break; }
    case 3: { console.log(tanggal, "Maret", tahun); break; }
    case 4: { console.log(tanggal, "April", tahun); break; }
    case 5: { console.log(tanggal, "Mei", tahun); break; }
    case 6: { console.log(tanggal, "Juni", tahun); break; }
    case 7: { console.log(tanggal, "Juli", tahun); break; }
    case 8: { console.log(tanggal, "Agustus", tahun); break; }
    case 9: { console.log(tanggal, "September", tahun); break; }
    case 10: { console.log(tanggal, "Oktober", tahun); break; }
    case 11: { console.log(tanggal, "November", tahun); break; }
    case 12: { console.log(tanggal, "Desember", tahun); break; }
}


//soal 3
var n = 5;
var string = '';
if (n > 0) {
    for (var baris = 0; baris < n; baris += 1) {
        string += '#'
        console.log(string);
    }
} else {
    console.log("nilai n harus lebih dari 0");
}



//soal 4
var m = 10;
if (m > 0) {
    for (index = 0; index < m; index++) {
        var module = index % 3;
        switch (module) {
            case 0: { console.log((index + 1) + "  - I love programming"); break; }
            case 1: { console.log((index + 1) + "  - I love Javascript"); break; }
            case 2: {
                console.log((index + 1) + "  - I love VueJS");
                console.log("===");
                break;
            }
        }
    }
} else {
    console.log("nilai n harus lebih dari 0");
}

