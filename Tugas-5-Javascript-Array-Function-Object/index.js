// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewanSorted = daftarHewan.sort();
for (var index = 0; index < daftarHewanSorted.length; index++){
    console.log(daftarHewan[index]);
}



//soal 2
function introduce(data) {
    var data = "Nama saya " +data.name+ " umur saya " +data.age+ 
    " tahun, alamat saya di " +data.address+ ", dan saya punya hobby yaitu "+data.hobby;
    return data;
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data);

console.log(perkenalan);



//soal 3
var vokal = ['a','i','u','e', 'o'];
function  hitung_huruf_vokal(word){
    
    word = word.toLowerCase();
    var arrayWord = word.split("");
    var arrayVokal = arrayWord.filter(function(char){
        return vokal.includes(char);
    });
    var counts = arrayVokal.length;

    return counts

}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1, hitung_2);




//soal 4
//catatan suku pertama adalah 0;
//angka >= 0 
function hitung(angka){
    
    var selisih = angka - 0;
    var dasar = -2; 
    var beda = selisih + dasar;
    var result = angka + beda;
    return result; 
}

var result = hitung(0);console.log(result);
var result = hitung(1);console.log(result);
var result = hitung(2);console.log(result);
var result = hitung(3);console.log(result);
var result = hitung(5);console.log(result);
var result = hitung(6);console.log(result);
