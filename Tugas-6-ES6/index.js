//soal 1
const luasKelilingPersegi =(panjang,lebar)=>{
    const luas = panjang * lebar
    const keliling = 2*(panjang +lebar)
    return [luas, keliling]
}

const [luasPersegi, kelilingPersegi] = luasKelilingPersegi(4,5)

console.log(luasPersegi);
console.log(kelilingPersegi);


//soal 2

 const newFunction = (firstName, lastName)=>{
     fullName = `${firstName} ${lastName}`
     return console.log(fullName);
 } 

newFunction("William", "Imoh");
   


 // soal 3
 const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

  const {firstName, lastName, address, hobby} = newObject;
  console.log(firstName, lastName, address, hobby)



  //soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined);


//soal 5
const planet = "earth" 
const view = "glass" 

let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`

console.log(before);